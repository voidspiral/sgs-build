# sgs-build

<img src="design/logo%20icon.png" align="right"/>

`sgs-build` is a global node.js command line tool that allows you to build SGS documents from a directory of markdown documents and a connection to the SGS web app. You can include Build API queries in your markdown documents and `sgs-build` will fetch, complile, format, and insert them.

`sgs-build`'s output is a compiled markdown document. The proscribed workflow looks like this:

- [obsidian](https://obsidian.md/)
    - plugin: https://github.com/aidenlx/alx-folder-note
- `sgs-build`
- `manuscript.md`
- pandoc
- `manuscript.html`
- princeXML
- `manuscript.pdf`

## Installing

There are two ways of using `sgs-build`

### Manual

- clone repo
- run `npm link`
- open command prompt
- navigate to project root directory (the one with `config.json`)
- run `sgs-build` in console

### NPM

coming soon.

## Project Structure

The builder anticipates a directory structure like a book, where chapters are top-level directories, sections are lower-level directories, and topics are individual markdown documents.

Specific folders can be blacklisted in the config file below, so that their contents are not included in the book.

Example

- 📁 Root Dir (dropbox)
    - 🎛️ config.json
    - 📁 Manuscript
        - 📁 Chapter 1
            - 📄 -about.md
            - 📄 1.md
            - 📄 2.md
    - 📁 build
        - 📄 manuscript.md

### Headings
The filenames are not used in the compiled text. You will need to put the heading in each file. The base heading level in each file should be H1 (`#`). `sgs-build` automatically manages the header depth based on the file's depth in the directory tree.

By default, it assumes that every folder is a section or chapter, even if it doesn't have an index file yet. If you plan to use folders to organize content without those folders representing sections, see the `collapseEmptyHeadings` option below.

### File Order

Files are generally ordered alphabetically based on file name. The exception is the folder index file, which is always before child documents in the folder. The folder index file is like the first section of a chapter, or section overview.

## config.js

when `sgs-build` runs, it checks the target directory for `config.json`.

- `targetDir`: String, optional. The subdirectory holding the manuscript markdown files.
- `blacklist`: List of strings. Filenames including this string will be ignored.
- `indexFiles`: Object,
    - `filename`: String. Name of index file related to a folder. Examples: `-about.md`, `index.md`, `_index.md`
    - `indexIsChild`: Bool, true. When true, searches the folder for index file. When false, searches the parent directory for the index file.
        - If you set this to false, it implies `useFolderName: true`.
    - `useFolderName`: Bool, false. When true, will use the name of the folder as the index filename, instead of `filename`.
- `outputName`: String. Path and filename of output, starting from the target directory. Don't include leading `./` or `/`.
    - **Warning: if you build your project into your manuscript folder, you'll have a recursion loop executing each build.**
- `collapseEmptyHeadings`: Bool, false. If true, folders that do not have index files will not be counted towards the heading depth of documents.
- `buildApiKeys`: Object. Property names are doc id strings, values are Build API keys from the Edit tab.
- `backfillApiCalls`: Bool, false. If true, `sgs-build` will populate Build API queries back into the original files. You might want this if you want to be able to easily target headings from inserted content, such as the name of character stat blocks.
    - **Warning: this alters the original files. Backup your files first.**
- `writeOutputEvenWithErrors`: Bool, false. When false, `sgs-build` will *not* write to the output file if there are errors. When true, it will write even if there are are errors. It's recommended to leave this false and actually fix the errors.
- `serverApiUrl`: String, `https://sgs-web.herokuapp.com/api/build?`. This is mostly for debugging the script.

### example

```json
{
    "targetDir": "manuscript",
    "blacklist": [
        "_notes",
        "_media",
        "_meta",
        ".obsidian"
    ],
    "indexFiles": {
        "filename": "-about.md",
        "indexIsChild": true,
        "useFolderName": false
    },
    "output": "build/manuscript.md",
    "collapseEmptyHeadings": true,
    "buildApiKeys": {
        "6130ee328ebbd0553c67cf7b": "2d905fa16a14ed074566f046f882ee4f",
        "622a7ffd79951b0022aae00a": "0da50684fffd34142b2912b21fc16d24"
    },
    "backfillApiCalls": false,
    "writeOutputEvenWithErrors": true
}
```

## Internal Links

We assume you're using Obsidian to write, and Pandoc for compiling markdown to HTML. Obsidian makes extensive use of wikilinks while pandoc supports internal links to headings via the `+auto_identifiers` and `+implicit_header_references` options. `sgs-build` therefore transforms the latter to the former.

Obsidian Link Formats

- `[[filename]]`
- `[[filename|with title]]`
- `[[filename#heading]]`
- `[[filename#heading|with title]]`

Link title priority

- explicit link title
- explicit heading
- first heading in the target file

## Build Api Call Replacements

The point of `sgs-build` is to allow you to easily make books from content on the [SGS web app](https://voidspiral.com/product/sgs/sgs/). For it to serve that purpose, it must be able to retrieve data from the web app. It does so by replacing api queries it finds in the markdown text with the results of those queries against the SGS web app server.

Each doc on the web app has a section for the Build API at the bottom of the Edit tab. There, you can generate, regenerate, or remove API keys that allow users to query the document.

Right now, `sgs-build` only supports markdown and html data from the Build Api, but eventually we will support some method of [user-generated json parsing](https://gitlab.com/voidspiral/sgs-build/-/issues/7).

**Warning: do not put your API key directly into the query in your markdown.** Instead, use the config file. Eventually, we intend to allow users to [store their API keys in environment variables](https://gitlab.com/voidspiral/sgs-build/-/issues/6), so they're better protected.

### Build API 

The SGS Build Api takes a number of optional parameters and returns data.

Build API calls are contained within a pair of comments that begin with `api`. Calls can either be a full URL copied from the Print Tab, or they can be newline-separated key-value pairs, which are easier to read and modify for humans. The API call is included in the output, but it's rewritten to key-value pairs. You can copy that from the output to your source files if it helps.

- `docId`: Required. The ID of the document you're getting data from.
- `contentType`: `statBlocks` | `entityCards` | `itemTable` | `rules`. Required. Sets what kind of data to be returned from the document.
    - `statBlocks` focuses on entity stat blocks, suitable for book printing.
    - `entityCards` focuses on entities, but more compactly. Designed for printing pages of entities for the GM to use during game.
    - `itemTable` is an abbreviated list of entities. It doesn't necessarily show all information, so it's better for simple entities like items and spells.
    - `rules` does not show entities. It shows the game rules instead, including stats, skills, tracks, and limitations.
- `contentFormat`: `markdown` | `HTML` (default) | `JSON`. Sets what format the data is returned in. At the moment, JSON isn't as useful because we haven't yet provided a system for custom parsers.
- `sortBy`: `xp` | `xpReversed` | `name` | `tags`. Sets the order of entities, when entities are included.
- `whitelistTags`: Comma separated list of tags. Only entities with one or more of these tags will be included in the output.
- `blacklistTags`: Comma separated list of tags. Entities with any one of these tags will be excluded from the output.
- `includeEntities`: Comma separated list of names. Only entities with names in this list will be included in the output.
- `excludeEntities`: Comma separated list of names. Entities with names in this list will be excluded from the output.
- `hideHeader`: `true` | `false` (default). If true, then no header will be included in the output. Useful if you want to include just a stat block in some content, in conjunction with `includeEntities`.

#### Example Build API call
```html
<!--api 
	docid=6130ee328ebbd0553c67cf7b
	contenttype=statblocks
	contentformat=markdown
	sortby=xpreversed
	whitelisttags=monster,item
	blacklisttags=incomplete,pc
	includeentities=cloud,squall
	excludeentities=kevin,bob
	hideHeader=true
-->
content here will be replaced with the returned data.
<!--/api-->
```