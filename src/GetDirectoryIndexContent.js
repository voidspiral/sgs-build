import fs from 'fs';
import ProcessLines from './ProcessLines.js';

function GetDirectoryIndexContent(filepath, options, depth) {
    let contents = '';
    let indexPath = ''
    if(!options.indexFiles) {
        indexPath = filepath + '/-about.md';
    }
    else if (options.indexFiles.useFolderName === true) {
        indexPath += '.md'
    }
    else if(!!options.indexFiles.filename){
        indexPath = filepath + '/' + options.indexFiles.filename
    }
    // console.log(indexPath);
    if(fs.existsSync(indexPath)){
        console.log(indexPath);
        contents = fs.readFileSync(indexPath, {encoding: 'utf-8', flags: 'r'});
        contents = ProcessLines(contents, depth, options, indexPath);
    }
    return contents;
}

export default GetDirectoryIndexContent;