// This module would be bad in a server environment, but this is a build script, not a server.
// https://www.npmjs.com/package/sync-request
import request from 'sync-request';

function DownloadData(url, options, filepath, callIndex) {
    try {
        const res = request('POST', url, {
            'user-agent': 'sgs-build',
            'timeout': 3000,
        })
        if(res.statusCode >= 300) {
            options.errors.push('Build API error\n\tstatus code ' + res.statusCode + '\n\t' + res.body);
            return '';
        }
        console.log(res)
        let data = res.getBody('utf-8');
        try {
            data = JSON.parse(data);
        }
        catch (e) {
            options.errors.push('Build API error\n\treturned data was not JSON.\n\t' + data + '\n\t' + e);
        }
        // console.log(data);
        if(data.error) {
            options.errors.push('Build API Server Error\n\t' + data.error + '\n\t' + filepath + '\n\tApi Call Number ' + callIndex);
            return '';
        }
        return data;
    }
    catch(e) {
        options.errors.push('Build API Server Call Error\n\t' + e)
        return '';
    }
}

export default DownloadData;