function PandocHeadingUrl (heading) {
    if(!heading) return '';
    heading = heading.toLowerCase();
    heading = encodeURI(heading);
    return heading;
}

export default PandocHeadingUrl;