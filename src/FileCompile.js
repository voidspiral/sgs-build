import fs from 'fs';
import ProcessLines from './ProcessLines.js';

function FileCompile(filepath, options, depth) {
    var output = '';
    console.log(filepath)
    if(fs.existsSync(filepath)){
        let content = fs.readFileSync(filepath, {encoding: 'utf8', flag: 'r'});
        // console.log(content)
        output += '\n\n';
        output += ProcessLines(content, depth, options, filepath);
        output += '\n\n';
    }
    return output;
}

export default FileCompile;