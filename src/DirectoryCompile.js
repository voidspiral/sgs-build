import fs from 'fs';
import path from 'path';
import FileCompile from './FileCompile.js';
import GetDirectoryIndexContent from "./GetDirectoryIndexContent.js";
import IsIndexFile from './IsIndexFile.js';

function DirectoryCompile (filepath, options, depth) {
    if(options && options.collapseEmptyHeadings === true) {
        depth = Math.max(0, depth - 1);
    }
    let output = '';
    output += GetDirectoryIndexContent(filepath, options, depth);
    console.log(filepath);
    var contents = fs.readdirSync(filepath, {withFileTypes: true });
    contents.forEach(dirent => {
        if(!!options.blacklist && options.blacklist.length > 0) {
            let listed = options.blacklist.reduce((prev, curr) => prev || filepath.includes(curr), false);
            if(listed) {
                return false;
            }
        }
        if(IsIndexFile(dirent.name, options)) {
            // console.log('was index');
        }
        else if(dirent.isDirectory() === true) {
            output += DirectoryCompile(filepath + '/' + dirent.name, options, depth + 1);
        }
        else {
            output += FileCompile(filepath + '/' + dirent.name, options, depth + 1);
        }
    })
    output += '\n\n';
    return output;
}

export default DirectoryCompile;