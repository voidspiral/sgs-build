#! /usr/bin/env node

import path from 'node:path';
import fs from 'fs';
import { fileURLToPath } from 'node:url';
import DirectoryCompile from './DirectoryCompile.js';
import ReplaceDummyLinks from './ReplaceDummyLinks.js';
const __dirname = path.dirname(fileURLToPath(import.meta.url));
console.log(__dirname)
const defaultConfig = JSON.parse(fs.readFileSync(__dirname + '/defaultConfig.json', {encoding: 'utf8', flag: 'r'}));

console.log('SGS Build Beginning');
console.log('===================');

// args and config
const cwd = process.cwd();
const userArgs = process.argv.slice(2);
const targetDir = userArgs.length > 0 ? userArgs[0].replaceAll('\\','/') : cwd.replaceAll('\\','/');
const configPath = targetDir + '/' + 'config.json';
const options = fs.existsSync(configPath) ? JSON.parse(fs.readFileSync(configPath, 'utf8')) : defaultConfig;

// setup global vars
options.firstHeadingPairs = [];
options.allHeadings = [];
options.allLinks = [];
options.errors = [];
options.warnings = [];
options.serverApiUrl = options.serverApiUrl || 'https://sgs.voidspiral.com/api/build?'
if(!options.serverApiUrl.endsWith('?')){
    options.serverApiUrl = options.serverApiUrl + '?';
}

// perform recursive compile
var msDir = targetDir;
if(!!options.targetDir) {
    msDir += '/' + options.targetDir;
}
let content = DirectoryCompile(msDir, options, -1);

// program output
console.log('===================');
let outputPath = options.output || 'build/manuscript.md';
outputPath = targetDir + '/' + outputPath;
// console.log(options.firstHeadingPairs[0]);
// console.log(options.allHeadings);
content = ReplaceDummyLinks(content, options);
if(options.warnings.length > 0) console.log('\nWarnings')
options.warnings.forEach(warning => {
    console.warn(warning);
})
if(options.errors.length > 0) console.log('\nErrors')
options.errors.forEach(err => {
    console.error(err);
})
if(options.errors.length === 0 || options.writeOutputEvenWithErrors === true) {
    console.log('Writing output file', outputPath);
    fs.writeFileSync(outputPath, content);
}
else {
    console.error('There were errors. Ouput file not generated.')
}
console.log('SGS Build Complete');
console.log('===================');