import PandocHeadingUrl from './PandocHeadingUrl.js';

function ReplaceDummyLinks(content, options) {
    options.allLinks.forEach(link => {
        let targetFile = link.filename + '.md';
        let headerOfFile = options.firstHeadingPairs.find(x => x.filepath.endsWith(targetFile));
        if(!!headerOfFile) {
            let newLink = headerOfFile.heading;
            let title = link.title || link.heading || newLink;
            let target = link.heading || newLink;
            newLink = '[' + title + '](#' + PandocHeadingUrl(target) + ')';
            content = content.replace(link.replacement, newLink);
        }
        else {
            options.warnings.push('Link missing target\n\t' + link.source + '\n\t' + link.original);
            content = content.replace(link.replacement, link.original);
        }
    })
    return content;
}

export default ReplaceDummyLinks;