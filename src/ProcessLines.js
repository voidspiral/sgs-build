import DummyLink from "./DummyLink.js";
import path from 'node:path';
import ProcessApiQuery from "./ProcessApiQuery.js";
import fs from 'fs';

function ProcessLines(content, depth, options, filepath) {
    if(!content) return '';

    // store fontmatter in case we need it later
    const frontmatterRegex = /(^---\n)([\S\s]+?)(\n---\n)/;
    let yaml = '';
    if(frontmatterRegex.test(content)){
        yaml = content.match(frontmatterRegex)[2];
        console.log('frontmatter:', yaml);
        content = content.replace(frontmatterRegex, '');
    }

    // transform Build API calls
    const apiRegex = /\<!\-\-api (.+?)\-\-\>([^\<]+?\<!\-\-\/api\-\-\>)?/gms;
    let apiCalls = [...content.matchAll(apiRegex)];
    if(apiCalls.length > 0) {
        apiCalls.forEach((call, index) => {
            content = ProcessApiQuery(call, content, options, filepath, index + 1);
        })
        // if backfilling, save content
        if(options.backfillApiCalls === true) {
            try {
                let backfill = content;
                if(yaml !== '') {
                    backfill = '---\n' + yaml + '\n---\n' + backfill;
                }
                fs.writeFileSync(filepath, backfill, {encoding: 'utf8', flag: 'w'})
            }
            catch (e) {
                options.errors.push('Backfill Api Call Error\n\t' + e)
            }
        }
    }
    
    // iterate over each line
    let output = '';
    let lines = content.split('\n');
    const headingRegex = /^#+ \w/;
    lines.forEach(line => {
        let wasFirst = true;
        let transformed = '';


        // line is heading
        if(headingRegex.test(line)) {
            // store headings
            let heading = line.replaceAll('#','').trim();
            options.allHeadings.push(heading)
            if(wasFirst) {
                options.firstHeadingPairs.push({
                    filepath,
                    heading
                });
            }
            wasFirst = false;
            // do depth alteration
            transformed = '#'.repeat(depth) + line;
        }

        // line is not heading
        else {
            transformed = line;
            // get links in line
            let links = [...transformed.matchAll(/\[\[.+?\]\]/g)];
            if(links.length > 0) {
                links.forEach(match => {
                    let link = match[0];
                    let dummy = DummyLink(link, options, filepath);
                    transformed = transformed.replace(link, dummy);
                })
            }
        }
        output += transformed + '\n';
    });


    return output;
}

export default ProcessLines;