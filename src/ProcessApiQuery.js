import DownloadData from './DownloadData.js';

function ProcessApiQuery(call, content, options, filepath, callIndex) {
    let originalCall = call[0];
    let apiQuery = call[1].trim();
    if(!!apiQuery) {
        if(apiQuery.includes('key=')) {
            options.warnings.push('Privacy Warning\n\tBuild API Key may have been included in plaintext markdown file.\n\t' + filepath + '\n\tApi Call Number ' + callIndex);;
        }
        let docId = '';
        let pairs = [];
        // query url
        if(apiQuery.startsWith('http')) {
            let split = apiQuery.split('?');
            pairs = split[1].split('&');
            if(!apiQuery.startsWith(options.serverApiUrl)) {
                options.warnings.push('Api Query Warning\n\tA Build API query targetted a server other than the one in options.\n\tQuery Target: ' + split[0] + '\n\tOptions Target: ' + options.serverApiUrl + '\n\t' + filepath + '\n\tApi Call Number ' + callIndex);
            }
        }
        // query data only
        else {
            pairs = apiQuery.split('\n').map(x => x.trim());
        }
        // console.log(pairs)
        let docProp = pairs.find(x => x.startsWith('docid'));
        if(!docProp) {
            options.errors.push('API Call Missing docid\n\t' + filepath + '\n\tApi Call Number ' + callIndex);
            return content;
        }
        else {
            docId = docProp.split('=')[1];
        }
        // add API key
        let key = options.buildApiKeys[docId];
        if(!key) {
            options.errors.push('No Build API Key found in config.json\n\tdocid: ' + docId + '\n\t' + filepath + '\n\tApi Call Number ' + callIndex);
            return content;
        }
        pairs = pairs.filter(x => !x.startsWith('key='));
        apiQuery = options.serverApiUrl + pairs.join('&');
        apiQuery += '&key=' + key;
        console.log(apiQuery)
        // actually call the query
        let callContent = DownloadData(apiQuery, options, filepath, callIndex);
        // receive data synchronously
        let callHeader = '<!--api \n\t' + pairs.join('\n\t') + '\n-->\n'
        let callFooter = '\n<!--/api-->'
        let output = callHeader + callContent + callFooter;
        return content.replace(originalCall, output);
    }
    return content;
}
export default ProcessApiQuery;