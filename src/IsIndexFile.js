import fs from 'fs';
import path from 'path';

function IsIndexFile(filepath, options) {
    // console.log(options);
    if(!filepath.endsWith('.md')) {
        return false;
    }
    if((!options || !options.indexFiles || !options.indexFiles.useFolderName) && filepath.includes('-about.md')) {
        return true;
    }
    else if (options.indexFiles.useFolderName === true) {
        let hypotheticalDir = filepath.replace('.md', '');
        if(fs.existsSync(hypotheticalDir)) {
            return true;
        }
        else {
            return false;
        }
    }
    else if(!!options.indexFiles.filename){
        let hypotheticalDir = path.basename(filepath);
        if(filepath.endsWith(options.indexFiles.filename) && fs.existsSync(hypotheticalDir)){
            return true;
        }
        else {
            return false;
        }
    }
    return false;
}

export default IsIndexFile;