function DummyLink(linkText, options, filepath) {
    const index = options.allLinks.length;
    const splitRe = /(?=[#\|])/g;
    const cleaned = linkText.replace('[[', '').replace(']]', '');
    const split = cleaned.split(splitRe);
    let filename = split[0];
    let heading = split.find(x => x.startsWith('#'))?.slice(1);
    let title = split.find(x => x.startsWith('|'))?.slice(1);
    let replacement = '{{link}}' + index + '{{/link}}';
    let linkObject = {
        original: linkText, 
        replacement,
        index,
        filename,
        heading,
        title,
        source: filepath,
    };
    options.allLinks.push(linkObject);
    return replacement;
}

export default DummyLink;